console.log('Happy monday?')

//fetch keyword

//syntax:
    //fetch('url',{options})
    //in option -method, body, and headers

    //GET post data

    fetch('https://jsonplaceholder.typicode.com/posts',)
    .then(res => {
        res.json()
        .then(result => {
            showPost(result)
          
        })
    })

    

    //Show post
       // We are going to create a function that will let us show the posts from out API.

       const showPost = (posts => {
           let entries = ``

           posts.forEach((post) => {
            entries += `
                 <div id="post-${post.id}">
                    <h3 id="title-${post.id}">${post.title}</h3>
                    <p id="body-${post.id}">${post.body}</p>
                    <button onclick = "editPost(${post.id})">Edit</button>
                    <button onclick= "deletePost(${post.id})">Delete</button>
                 </div>
            `
            
           })
            
           document.querySelector('#div-post-entries').innerHTML = entries
       })

      //POST DATA ON OUR API

      document.querySelector('#form-add-post').addEventListener('submit', (event)=> {
           event.preventDefault();
         


           //post.method
           fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body:JSON.stringify({
                title: document.querySelector('#txt-title').value,
                body: document.querySelector('#txt-body').value,
                userId: 1
            }),
            headers: {
                'content-type' : 'application/json'
            }
           }).then(res => res.json())
           .then(result =>{
            console.log(result)
            alert("SUCCESSFULLY ADDED")
             document.querySelector('#txt-title').value =null;
            document.querySelector('#txt-body').value =null;
            
           })
           
      })

      //EDIT POST

       const editPost = (id) => {
            
            let title = document.querySelector(`#title-${id}`).innerHTML
            let body = document.querySelector(`#body-${id}`).innerHTML

            document.querySelector('#txt-edit-title').value = title
            document.querySelector('#txt-edit-body').value = body
            document.querySelector('#txt-edit-id').value = id

            document.querySelector('#btn-submit-update').removeAttribute('disabled')

       }

       document.querySelector(`#form-edit-post`).addEventListener('submit',(event) => {
        event.preventDefault();
        let id = document.querySelector('#txt-edit-id').value;
        fetch(`https://jsonplaceholder.typicode.com/posts/#post-${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                title:document.querySelector('#txt-edit-title').value,
                id:id,
                body: document.querySelector('#txt-edit-body').value,
                userId:1
            })
        }).then(res => {
           return res.json()
        }).then(result => {
            console.log(result)
            alert("SUCCESSFULLY UPDATED")
            document.querySelector('#txt-edit-title').value=null;
            document.querySelector('#txt-edit-id').value =null;
            document.querySelector('#txt-edit-body').value =null;

            document.querySelector('#btn-submit-update').setAttribute('disabled', true)
        })    
       })

   
       //DELETE USERS
      function deletePost  (id) {
       fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
       }).then(res => {
        return res.json()
       }).then(result => {
         
          document.querySelector(`#post-${id}`).remove()
       })
     
    }